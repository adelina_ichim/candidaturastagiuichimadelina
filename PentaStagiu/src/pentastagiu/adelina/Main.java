package pentastagiu.adelina;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Enter the number associated with the figure desired:" + "\n" + "1 Circle" + "\n" + "2 Triangle" + "\n" + "3 Rectangle");
		
		Scanner scan = new Scanner(System.in);
		int choice = scan.nextInt();
				
		if (choice == 1) {
			System.out.println("Enter the radius value:");			
			double radius = scan.nextDouble();
			
			Circle circle = new Circle();
			circle.setRadius(radius);
			
			double area = circle.calculateArea();
			double perimeter = circle.calculatePerimeter();
			
			System.out.println("Circle area: " + area);
			System.out.println("Circle perimeter: " + perimeter);
		} 
		
		else if (choice == 2){
			System.out.println("Enter the sideA value:");
			double sideA = scan.nextDouble();
			
			System.out.println("Enter the sideB value:");
			double sideB = scan.nextDouble();
			
			System.out.println("Enter the sideC value:");
			double sideC = scan.nextDouble();
			
			Triangle triangle = new Triangle();
			
			triangle.setSideA(sideA);
			triangle.setSideB(sideB);
			triangle.setSideC(sideC);
			
			double perimeterTriangle = triangle.calculatePerimeter();
			double areaTriangle = triangle.calculateArea();
			
			System.out.println("Triangle perimeter: " + perimeterTriangle);
			System.out.println("Triangle area: " + areaTriangle);
			
			//System.out.println(sideA + " " + sideB + " " + sideC);
		}
		
		else if (choice == 3){
			System.out.println("Enter the width value:");
			double width = scan.nextDouble();
			
			System.out.println("Enter the length value:");
			double length = scan.nextDouble();
			
			Rectangle rectangle = new Rectangle();
			
			rectangle.setWidth(width);
			rectangle.setLength(length);
			
			double perimeterRectangle = rectangle.calculatePerimeter();
			double areaRectangle = rectangle.calculateArea();
			
			System.out.println("Rectangle perimeter: " + perimeterRectangle);
			System.out.println("Rectangle area: " + areaRectangle);
		}
		
	}

}
