package pentastagiu.adelina;

public class Rectangle extends Figure {
	private double width;
	private double length;
	
	public void setWidth(double value){
		this.width = value;
	}
	
	public void setLength(double value){
		this.length = value;
	}
	
	public double calculateArea(){
		double area = width * length;
		return area;
	}
	
	public double calculatePerimeter(){
		double perimeter = 2 * width + 2 * length;
		return perimeter;
	}
}
