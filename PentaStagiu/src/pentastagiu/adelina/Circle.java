package pentastagiu.adelina;

public class Circle extends Figure{
	
	private double radius;
	
	public void setRadius (double value){
		this.radius = value;
	}
	
	public double calculateArea(){		
		double area;
		area = Math.PI * Math.pow(radius, 2);
		return area;
	}
	
	public double calculatePerimeter(){
		double perimeter;
		perimeter = 2 * Math.PI * radius;
		return perimeter;
	}
}
