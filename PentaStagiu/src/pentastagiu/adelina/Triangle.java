package pentastagiu.adelina;

public class Triangle extends Figure{
	private double sideC;
	private double sideA;
	private double sideB;
	
	public void setSideA(double value){
		this.sideA = value;
	}
	
	public void setSideB(double value){
		this.sideB = value;
	}
	
	public void setSideC(double value){
		this.sideC = value;
	}
	
	public double calculateArea(){
		double perimeter = this.calculatePerimeter();
		double area = Math.sqrt(perimeter/2 * (perimeter/2 - sideA) * (perimeter/2 - sideB) * (perimeter/2 - sideC));
		return area;
	}
	
	public double calculatePerimeter(){
		double perimeter = sideC + sideA + sideB;
		return perimeter;
	}
	
}
